import boto3
import os
import shutil
import sys

# s3 bucket name
aws_bucket = sys.argv[1]

path = '/tmp/archive_cache_db/'
db = "cache.db"

# backup the db to /tmp/bak_cache_db, so we need to create the dir first
os.system("mkdir -p " + path)

cmd = "sqlite3 " + db + " .dump > '" + path + "''" + db + "'"

# do the db dump
os.system(cmd)

# configuring filepath and tar file name
archive_name = db
db_path = path + db
archive_path = path + archive_name

print('[FILE] Creating archive for ' + db)
shutil.make_archive(archive_path, 'gztar', path)
print('Completed archiving database')
full_archive_file_path = archive_path + ".tar.gz"
object_name = archive_name + ".tar.gz"

# S3 Connection
s3 = boto3.resource('s3')

# put it up!
response = s3.Object(aws_bucket, object_name).upload_file(full_archive_file_path)
