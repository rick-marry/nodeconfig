import boto3
import os
import tarfile
import sys

# s3 bucket name
aws_bucket = sys.argv[1]

path = '/tmp/restore_cache_db/'
db = "cache.db"

# backup the db to /tmp/bak_cache_db, so we need to create the dir first
os.system("mkdir -p " + path)

# configuring filepath and tar file name
archive_name = db
db_path = path + db
archive_path = path + archive_name

full_archive_file_path = archive_path + ".tar.gz"
object_name = archive_name + ".tar.gz"

s3 = boto3.resource('s3')
bucket = s3.Bucket(aws_bucket)

objs = list(bucket.objects.filter(Prefix=object_name))
if len(objs) > 0 and objs[0].key == object_name:
    print("Cache db backup archive exists.  We will therefore, hydrate the db with it.")
    # bring it down!
    response = s3.Object(aws_bucket, object_name).download_file(full_archive_file_path)

    os.chdir(path)
    tar = tarfile.open(full_archive_file_path)
    tar.extractall()
    tar.close()

    os.chdir("/home/ec2-user")

    # delete the current cache.db and overwrite it with the restored archive from s3
    cmd = "rm " + db + " && sqlite3 " + db + " < " + archive_path + " && chown ec2-user:ec2-user " + db

    # do the restore
    os.system(cmd)
else:
    print("Cache db backup archive doesn't exist in the s3 bucket. This is ok.. we are starting new!")


